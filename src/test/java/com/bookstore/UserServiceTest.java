package com.bookstore;

import java.util.Date;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import com.bookstore.model.UserRequest;
import com.bookstore.repository.UsersRepository;
import com.bookstore.service.UserService;

@SpringBootTest
public class UserServiceTest {

	@Mock
	UsersRepository usersRepositoryMock;
	@InjectMocks
	UserService userService;

	UserRequest user;
	
	public void createUser(){
		user = new UserRequest();
		user.setUsername("john.doe");
		user.setPassword("password");
		user.setDateOfBirth(new Date());
	}
	
	public void mockRepo(){
//		Mockito.doReturn( user ).when( usersRepositoryMock ).save( Mockito.any() );
//		Mockito.doReturn( user ).when( usersRepositoryMock ).findByUserName( Mockito.anyString() );
	}
	
	@Before
	public void init() throws Exception {
		createUser();
		MockitoAnnotations.initMocks( this );
		mockRepo();
	}
	

	
	@Test
	public void shouldReturnSuccess(){
		
		
	}
}
