package com.bookstore.config;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.bookstore.Util.EncryptUtil;
import com.bookstore.entity.OrdersEntity;
import com.bookstore.entity.UsersEntity;
import com.bookstore.repository.OrdersRepository;
import com.bookstore.repository.UsersRepository;

@Configuration
public class LoadDatabase {

	private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

	@Bean
	public CommandLineRunner initData(UsersRepository usersRep, OrdersRepository ordersRep) {
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
		return (args) -> {
			log.info("------------------------INIT DB DATA ----------------------------------");

			EncryptUtil encrypt = new EncryptUtil();
			byte[] passSalt = encrypt.generateSalt();
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy", new Locale("en", "US"));
			UsersEntity user = new UsersEntity();
			user.setUsername("john.doe");
			user.setName("john");
			user.setSurname("doe");
			user.setPassword(encrypt.getEncryptedPassword("password", passSalt));
			user.setPassSalt(passSalt);
			user.setDateOfBirth(df.parse("15/01/1985"));
			user = usersRep.save(user);

			OrdersEntity order1 = new OrdersEntity();
			order1.setBookId(1L);
			order1.setUsersEntityOrder(user);
			ordersRep.save(order1);

			OrdersEntity order2 = new OrdersEntity();
			order2.setBookId(4L);
			order2.setUsersEntityOrder(user);
			ordersRep.save(order2);
		};
	}
}