package com.bookstore.model;

import java.io.Serializable;


public class LoginResponse implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 437505030358443557L;
	
	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
