package com.bookstore.model;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonProperty;


public class CallPubisherBook implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6725920312322263002L;


	public CallPubisherBook() {
		this.isRecommended=false;
	}

	@JsonProperty("id")
	protected Long id;

	@JsonProperty("author_name")
	protected String authorName;

	@JsonProperty("book_name")
	protected String bookName;

	@JsonProperty("price")
	protected Double price;
	
	@JsonProperty("is_recommended")
	protected Boolean isRecommended;

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Boolean getIsRecommended() {
		return isRecommended;
	}

	public void setIsRecommended(Boolean isRecommended) {
		this.isRecommended = isRecommended;
	}

	
}
