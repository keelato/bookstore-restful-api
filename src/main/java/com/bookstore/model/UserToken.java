package com.bookstore.model;

import java.io.Serializable;
import java.util.Date;
import org.springframework.data.annotation.Id;

public class UserToken implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1322510928366462318L;

	
	@Id
	private String id;

	private Long expiration;
	
	private String username;
	
	private Date date;

}
