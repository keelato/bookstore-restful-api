package com.bookstore.model;

import java.io.Serializable;


public class OrdersResponse implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -1066877705792021635L;
	
	private Double price;

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}
	
}
