package com.bookstore.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;



public class OrdersRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3515097288000615712L;
	
	private List<Long> orders = new ArrayList<>();

	public List<Long> getOrders() {
		return orders;
	}


	public void setOrders(List<Long> orders) {
		this.orders = orders;
	}


}
