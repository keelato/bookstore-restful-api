package com.bookstore.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.bookstore.model.CallPubisherBook;
import com.bookstore.model.LoginRequest;
import com.bookstore.model.LoginResponse;
import com.bookstore.model.OrdersRequest;
import com.bookstore.model.OrdersResponse;
import com.bookstore.model.UserRequest;
import com.bookstore.model.UserResponse;
import com.bookstore.service.CallPubisherService;
import com.bookstore.service.OrdersService;
import com.bookstore.service.UserService;


@RestController
public class BookStoreController {
	
	@Autowired
    private UserService userService;
	
	@Autowired
	private CallPubisherService callPubisherService;
	
	@Autowired
	private OrdersService ordersService;

	@PostMapping("/login")
	@ResponseBody
	public LoginResponse login(@RequestBody LoginRequest req) throws Exception {
		return userService.signin(req.getUsername(), req.getPassword());
	}

	@GetMapping("/users")
	@ResponseBody
	public UserResponse getUsers(HttpServletRequest request) throws Exception {
		return userService.getUser(request);
	}
	
	@DeleteMapping("/users")
	@ResponseBody
	public void deleteUsers(HttpServletRequest request) throws Exception {
		 userService.deleteUser(request);
	}

	@PostMapping("/users")
	@ResponseBody
	public void createUsers(@RequestBody UserRequest req) throws Exception {
		userService.createUser(req);
	}
	
	@PostMapping("/users/orders")
	@ResponseBody
	public OrdersResponse getOrders(HttpServletRequest request, @RequestBody OrdersRequest orderReq) throws Exception {
		return ordersService.saveOrders(request, orderReq);
	}
	
	@GetMapping("/books")
	@ResponseBody
	public List<CallPubisherBook> getBooks() throws Exception {
		return callPubisherService.getAllBookPubisher();
	}
	
}
