package com.bookstore.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name = "ORDERS")
public class OrdersEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4078396695139512676L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private Long bookId;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    @JoinColumn(name = "refUserId", nullable = false, referencedColumnName = "userId")
    private UsersEntity usersEntity;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getBookId() {
		return bookId;
	}

	public void setBookId(Long bookId) {
		this.bookId = bookId;
	}

	public UsersEntity getUsersEntityOrder() {
		return usersEntity;
	}

	public void setUsersEntityOrder(UsersEntity usersEntityOrder) {
		this.usersEntity = usersEntityOrder;
	}

}
