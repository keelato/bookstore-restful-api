package com.bookstore.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.bookstore.entity.OrdersEntity;
import com.bookstore.entity.UsersEntity;
import com.bookstore.exception.CustomException;
import com.bookstore.model.CallPubisherBook;
import com.bookstore.model.OrdersRequest;
import com.bookstore.model.OrdersResponse;
import com.bookstore.repository.OrdersRepository;
import com.bookstore.repository.UsersRepository;
import com.bookstore.security.JwtTokenProvider;

@Service
@Transactional(rollbackFor = Exception.class)
public class OrdersService {
	
	private static final Logger log = LoggerFactory.getLogger(OrdersService.class);

	@Autowired
	private OrdersRepository ordersRepository;

	@Autowired
	private UsersRepository usersRepository;

	@Autowired
	private CallPubisherService externalPubisherService;
	
	@Autowired
	private JwtTokenProvider jwtTokenProvider;

	public OrdersResponse saveOrders(HttpServletRequest request, OrdersRequest orders) throws Exception {
		
		log.debug("----- start : saveOrders -----");
		
		String token = jwtTokenProvider.resolveToken(request);
		String username = jwtTokenProvider.getUsername(token);
		
		UsersEntity user = usersRepository.findByUserName(username);
		
		if (null == user ) {
			throw new CustomException("Invalid token/token expire", HttpStatus.NOT_FOUND);
		} else {
			
			List<CallPubisherBook> books = externalPubisherService.getAllBookPubisher();
			List<OrdersEntity> dataIns = new ArrayList<>();
			Double price = 0D;
			
			for (Long bookId : orders.getOrders()) {
				CallPubisherBook bookEq = null;
				for (CallPubisherBook book : books) {
					if (book.getId().equals(bookId)) {
						bookEq = book;
						price += bookEq.getPrice();
						break;
					}
				}
				if (bookEq == null) {
					throw new CustomException("Book id not exist", HttpStatus.NOT_FOUND);
				}
				OrdersEntity order = new OrdersEntity();
				order.setBookId(bookId);
				order.setUsersEntityOrder(user);
				dataIns.add(order);
			}
			
			ordersRepository.saveAll(dataIns);
			OrdersResponse ordersResponse = new OrdersResponse();
			ordersResponse.setPrice(price);
			return ordersResponse;
		}
	}
}
