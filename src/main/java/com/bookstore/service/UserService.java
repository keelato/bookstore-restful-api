package com.bookstore.service;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.bookstore.Util.EncryptUtil;
import com.bookstore.entity.OrdersEntity;
import com.bookstore.entity.UsersEntity;
import com.bookstore.exception.CustomException;
import com.bookstore.model.LoginResponse;
import com.bookstore.model.UserRequest;
import com.bookstore.model.UserResponse;
import com.bookstore.repository.OrdersRepository;
import com.bookstore.repository.UsersRepository;
import com.bookstore.security.JwtTokenProvider;

@Service("userService")
@Transactional(rollbackFor = Exception.class)
public class UserService {

	private static final Logger log = LoggerFactory.getLogger(UserService.class);

	@Autowired
	private UsersRepository usersRepository;
	@Autowired
	private OrdersRepository ordersRepository;
	@Autowired
	private JwtTokenProvider jwtTokenProvider;

	public UserResponse getUser(HttpServletRequest request) throws Exception {

		log.debug("----- start : getUser -----");

		UserResponse userRes = new UserResponse();
		String token = jwtTokenProvider.resolveToken(request);
		String username = jwtTokenProvider.getUsername(token);
		UsersEntity user = usersRepository.findByUserName(username);
		userRes.setName(user.getName());
		userRes.setSurname(user.getSurname());
		userRes.setDateOfBirth(user.getDateOfBirth());

		List<Long> orders = new ArrayList<>();
		for (OrdersEntity order : user.getOrdersEntity()) {
			orders.add(order.getBookId());
		}

		userRes.setOrders(orders);
		return userRes;
	}

	public void deleteUser(HttpServletRequest request) {

		log.debug("----- start : deleteUserDetail -----");

		String token = jwtTokenProvider.resolveToken(request);
		String username = jwtTokenProvider.getUsername(token);
		UsersEntity user = usersRepository.findByUserName(username);

		if (null != user) {

			if (!user.getOrdersEntity().isEmpty()) {
				ordersRepository.deleteAll(user.getOrdersEntity());
			}
			usersRepository.delete(user);
		}else {
			throw new CustomException("Invalid token/token expire", HttpStatus.NOT_FOUND);
		}
	}

	public void createUser(UserRequest req) throws Exception {

		log.debug("----- start : createUser -----");

		if (StringUtils.isBlank(req.getUsername()) || StringUtils.isBlank(req.getPassword())
				|| req.getDateOfBirth() == null) {
			throw new CustomException("invalid data", HttpStatus.BAD_REQUEST);
		} else if (!StringUtils.contains(req.getUsername(), ".")) {
			throw new CustomException("require DOT example: name.surname", HttpStatus.BAD_REQUEST);
		} else {

			EncryptUtil encrypt = new EncryptUtil();
			byte[] passSalt = encrypt.generateSalt();
			String[] usernameSplit = StringUtils.split(req.getUsername(), ".", 2);
			UsersEntity newUser = new UsersEntity();
			newUser.setUsername(req.getUsername());
			newUser.setName(usernameSplit[0]);
			newUser.setSurname(usernameSplit[1]);
			newUser.setPassword(encrypt.getEncryptedPassword(req.getPassword(), passSalt));
			newUser.setPassSalt(passSalt);
			newUser.setDateOfBirth(req.getDateOfBirth());
			usersRepository.save(newUser);
		}

	}

	public LoginResponse signin(String username, String password) throws Exception {

		log.debug("----- start : signin -----");

		UsersEntity user = usersRepository.findByUserName(username);
		EncryptUtil encrypt = new EncryptUtil();

		if (null == user || (!encrypt.authenticate(password, user.getPassword(), user.getPassSalt()))) {
			throw new CustomException("Invalid username/password", HttpStatus.NOT_FOUND);
		}

		LoginResponse loginResponse = new LoginResponse();
		loginResponse.setToken(jwtTokenProvider.createToken(username));
		return loginResponse;

	}

}
