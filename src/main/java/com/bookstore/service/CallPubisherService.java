package com.bookstore.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.bookstore.Util.BookComparator;
import com.bookstore.model.CallPubisherBook;
import com.bookstore.model.CallPubisherBookRecomend;



@Service
@CacheConfig(cacheNames={"books"})
public class CallPubisherService {

	private static final Logger log = LoggerFactory.getLogger(CallPubisherService.class);
	
	private static String PUBISHERBOOKURL = "https://scb-test-book-publisher.herokuapp.com/books";
	private static String PUBISHERBOOKRECOMMENDATIONURL = "https://scb-test-book-publisher.herokuapp.com/books/recommendation";
	
	@Cacheable
	public List<CallPubisherBook> getAllBookPubisher() {	
		
		log.debug("----- start : getAllBookPubisher -----");
		return this.prepareAllBookPubisher();
	}

	@CachePut
	public List<CallPubisherBook> updateBookPubisherCache() {
		return this.prepareAllBookPubisher();
	}
	
	private List<CallPubisherBook> prepareAllBookPubisher() {
		List<CallPubisherBook> data = this.getBookRecommendationPubisher();
		for (CallPubisherBook book : this.getBookPubisher()) {
			if(!data.stream().filter(d -> d.getId().equals(book.getId())).findFirst().isPresent()) {
				data.add(book);
			}
		}
		Collections.sort(data, new BookComparator());
		return data;
	}
	
	public List<CallPubisherBook> getBookPubisher() {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<CallPubisherBook[]> response 
			= restTemplate.getForEntity(PUBISHERBOOKURL, CallPubisherBook[].class);
		return response.hasBody() ? new ArrayList<CallPubisherBook>(Arrays.asList(response.getBody())) : Collections.emptyList();
	}
	
	public List<CallPubisherBook> getBookRecommendationPubisher() {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<CallPubisherBookRecomend[]> response 
			= restTemplate.getForEntity(PUBISHERBOOKRECOMMENDATIONURL, CallPubisherBookRecomend[].class);
		return response.hasBody() ? new ArrayList<CallPubisherBook>(Arrays.asList(response.getBody())) : Collections.emptyList();
	}
	
}
