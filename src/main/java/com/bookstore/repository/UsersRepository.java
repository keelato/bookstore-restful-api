package com.bookstore.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.bookstore.entity.UsersEntity;

@Repository
public interface UsersRepository extends JpaRepository<UsersEntity, String> {
	
	@Query("SELECT u FROM USERS u WHERE u.username = ?1")
	UsersEntity findByUserName(String userName);
}