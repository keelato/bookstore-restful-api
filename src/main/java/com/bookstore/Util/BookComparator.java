package com.bookstore.Util;

import java.util.Comparator;
import com.bookstore.model.CallPubisherBook;

public class BookComparator implements Comparator<CallPubisherBook> {
	@Override
	public int compare(CallPubisherBook b1, CallPubisherBook b2) {
		int compareRec = b2.getIsRecommended().compareTo(b1.getIsRecommended());
		if (compareRec == 0) {
			return b1.getBookName().compareTo(b2.getBookName());
		} else {
			return compareRec;
		}
	}
}
