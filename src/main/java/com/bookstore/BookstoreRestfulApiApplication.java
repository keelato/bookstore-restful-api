package com.bookstore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching 
public class BookstoreRestfulApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookstoreRestfulApiApplication.class, args);
	}

}
