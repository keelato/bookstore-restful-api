package com.bookstore.scheduler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import com.bookstore.service.CallPubisherService;


@Component
public class SchedulerTask {

	@Autowired
	CallPubisherService callPubisherService;
	
	//update every day on 00.05
	@Scheduled(cron="5 0 * * *")
	public void midnightTask() throws Exception {
		callPubisherService.updateBookPubisherCache();
	}
	
}
