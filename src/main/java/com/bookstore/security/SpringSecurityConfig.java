package com.bookstore.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;



@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {
	
	private static final Logger log = LoggerFactory.getLogger(SpringSecurityConfig.class);

	@Autowired
	private JwtTokenProvider jwtTokenProvider;
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
//        http
//         .csrf().disable()
//         .authorizeRequests().anyRequest().authenticated()
//         .and()
//         .httpBasic();

	    http.csrf().disable();
	    http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	    http.authorizeRequests()//
	        .antMatchers("/login").permitAll()
	        .antMatchers(HttpMethod.POST,"/users").permitAll()
	        .antMatchers("/books").permitAll()
	        .anyRequest().authenticated();

	    http.exceptionHandling().accessDeniedPage("/login");
	    http.apply(new JwtTokenFilterConfigurer(jwtTokenProvider));

	}

	  @Override
	    public void configure(WebSecurity web) throws Exception {
	      // Allow swagger to be accessed without authentication
	      web.ignoring().antMatchers("/v2/api-docs")//
	          .antMatchers("/swagger-resources/**")//
	          .antMatchers("/swagger-ui.html")//
	          .antMatchers("/configuration/**")//
	          .antMatchers("/webjars/**")//
	          .antMatchers("/public")
	          // Un-secure H2 Database (for testing purposes, H2 console shouldn't be unprotected in production)
	          .and()
	          .ignoring()
	          .antMatchers("/h2-console/**/**");;
	    }
	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication().withUser("admin").password(passwordEncoder().encode("admin")).roles("GUEST");
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
}
